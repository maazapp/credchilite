package com.elc.credchilite

import android.app.Application
import android.text.TextUtils
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.elc.credchilite.Authentication.Registration.RegisterRepository
import com.google.firebase.auth.FirebaseUser

class RegisterViewModel(application: Application): AndroidViewModel(application) {
    var registerRepo: RegisterRepository
    var userMutableLiveData: MutableLiveData<FirebaseUser>
    var userEmail: MutableLiveData<String>
    init {
        registerRepo = RegisterRepository(application)
        userMutableLiveData = registerRepo.userMutableLiveData
        userEmail = registerRepo.userEmailMutableLiveData
    }

    fun registerUser(email: String, password: String){
        registerRepo.register(email,password)
    }

    fun validatePassword(email: String, password: String, confirmPassword: String): Boolean{
        val minimumLength = 6
        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password) && !TextUtils.isEmpty(confirmPassword)){
            if (password.length >= minimumLength && password.equals(confirmPassword)){
                return true
            }
        }
        return false
    }

    fun getUserLiveData(): MutableLiveData<FirebaseUser> = userMutableLiveData
}