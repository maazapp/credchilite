package com.elc.credchilite.Authentication.Login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.elc.credchilite.LoginViewModel
import com.elc.credchilite.R
import com.elc.credchilite.databinding.FragmentLoginBinding

class LoginFragment : Fragment() {

    private lateinit var navigate1: Button
    private lateinit var email: TextView
    private lateinit var password: TextView
    private lateinit var binding: FragmentLoginBinding

    private lateinit var viewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login,container, false)
        //Necessary for initializing the binding to VM
        binding.viewmodel = this.viewModel
        //In order to read values in XML files
        binding.setLifecycleOwner(this)
        val view = binding.root
        navigate1 = view.findViewById(R.id.navigate) as Button
        email = view.findViewById(R.id.email) as EditText
        password = view.findViewById(R.id.password) as EditText
        binding.navigate.setOnClickListener {
            findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToRegisterFragment())
        }
        binding.login.setOnClickListener {
            val emailText = email.text.toString()
            val passwordText = password.text.toString()
            viewModel.loginUser(emailText, passwordText)
        }
        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this).get(LoginViewModel::class.java)
        viewModel.userMutableLiveData.observe(this){ firebaseUser ->
            if (firebaseUser!=null){
                refreshCurrentFragment()
                Toast.makeText(this.activity,"Login success",Toast.LENGTH_LONG).show()
            }else{
                Toast.makeText(this.activity,"Invalid user",Toast.LENGTH_LONG).show()
            }
        }

    }

    private fun refreshCurrentFragment(){
        val navController = findNavController()
        val id = navController.currentDestination?.id
        navController.popBackStack(id!!,true)
        navController.navigate(id)
    }
}