package com.elc.credchilite.Authentication.Login

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class LoginRepository(application: Application) {
    var mAuth: FirebaseAuth
    var userMutableLiveData: MutableLiveData<FirebaseUser>
    var userEmailMutableLiveData: MutableLiveData<String>
    val app = application
    init {
        mAuth = FirebaseAuth.getInstance()
        userMutableLiveData = MutableLiveData()
        userEmailMutableLiveData = MutableLiveData()
    }

    fun login(email:String, password:String){
        mAuth.signInWithEmailAndPassword(email,password)
            .addOnCompleteListener{
                if (it.isSuccessful){
                    Log.i("Login result","Success")
                    userMutableLiveData.postValue(mAuth.currentUser)
                    userEmailMutableLiveData.postValue(mAuth.currentUser!!.email)
                }else{
                    Log.i("Login result","${it.result} failure")
                }
            }
    }
}