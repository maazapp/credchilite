package com.elc.credchilite.Authentication.Registration

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.elc.credchilite.R
import com.elc.credchilite.RegisterViewModel
import com.elc.credchilite.databinding.FragmentRegisterBinding

class RegisterFragment : Fragment() {
    private lateinit var emailEdt: EditText
    private lateinit var passwordEdt: EditText
    private lateinit var confirmPasswordEdt: EditText
    private lateinit var binding: FragmentRegisterBinding
    private lateinit var viewModel: RegisterViewModel
       override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
           binding = DataBindingUtil.inflate(inflater,R.layout.fragment_register,container,false)
           binding.viewmodel = this.viewModel
           binding.setLifecycleOwner(this)
           val view = binding.root
           emailEdt = view.findViewById(R.id.email) as EditText
           passwordEdt = view.findViewById(R.id.password) as EditText
           confirmPasswordEdt = view.findViewById(R.id.confirm_password) as EditText
           binding.navigate.setOnClickListener {
               findNavController().navigate(RegisterFragmentDirections.actionRegisterFragmentToLoginFragment())
           }
           binding.createUser.setOnClickListener {
               val emailText = emailEdt.text.toString()
               val passwordText = passwordEdt.text.toString()
               val confirmPasswordText = confirmPasswordEdt.text.toString()
               val isPasswordValid = viewModel.validatePassword(emailText, passwordText, confirmPasswordText)
               if (isPasswordValid){
                   viewModel.registerUser(emailText,passwordText)
               } else{
                   Toast.makeText(this.activity, "Invalid credentials", Toast.LENGTH_LONG).show()
               }
           }
        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this).get(RegisterViewModel::class.java)
        viewModel.userMutableLiveData.observe(this){firebase ->
            if (firebase!=null){
                refreshCurrentFragment()
                Toast.makeText(this.activity,"User created",Toast.LENGTH_LONG).show()
            }else{
                Toast.makeText(this.activity,"Invalid user",Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun refreshCurrentFragment(){
        val navController = findNavController()
        val id = navController.currentDestination?.id
        navController.popBackStack(id!!,true)
        navController.navigate(id)
    }
}