package com.elc.credchilite.Authentication.Registration

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class RegisterRepository(application: Application) {
    var mAuth: FirebaseAuth
    var userMutableLiveData: MutableLiveData<FirebaseUser>
    var userEmailMutableLiveData: MutableLiveData<String>
    val app = application

    init {
        mAuth = FirebaseAuth.getInstance()
        userMutableLiveData = MutableLiveData()
        userEmailMutableLiveData = MutableLiveData()
    }

    fun register(email: String, password: String){
        mAuth.createUserWithEmailAndPassword(email,password)
            .addOnCompleteListener{
                if (it.isSuccessful){
                    Log.i("Register result","Success")
                    userMutableLiveData.postValue(mAuth.currentUser)
                    userEmailMutableLiveData.postValue(mAuth.currentUser!!.email)
                }else{
                    Log.i("Register result","${it.result} failure")
                }
            }
    }
}