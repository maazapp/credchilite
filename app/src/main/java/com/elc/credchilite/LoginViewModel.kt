package com.elc.credchilite

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.elc.credchilite.Authentication.Login.LoginRepository
import com.google.firebase.auth.FirebaseUser

class LoginViewModel(application: Application): AndroidViewModel(application) {
    var loginRepository: LoginRepository
    var userMutableLiveData: MutableLiveData<FirebaseUser>
    var userEmail: MutableLiveData<String>

    init {
        loginRepository = LoginRepository(application)
        userMutableLiveData = loginRepository.userMutableLiveData
        userEmail = loginRepository.userEmailMutableLiveData
    }

    fun loginUser(email: String, password: String){
        loginRepository.login(email,password)
    }

    fun getUserLiveData(): MutableLiveData<FirebaseUser> = userMutableLiveData
}